using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PDIdle : StateMachineBehaviour
{
    private PlayerController _playerController;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _playerController = animator.GetComponent<PlayerController>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Input.GetMouseButtonDown(1))
        {

            Vector2 mousePosition = Input.mousePosition;
            _playerController._moveTarget = _playerController._cam.ScreenToWorldPoint(mousePosition);

            _playerController.ChangeScale(_playerController._moveTarget);
            _playerController.ChangeState("Move");
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
