using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourAttackBase : MonoBehaviour
{

    private Animator _relatedAnimator;
    public float cooldown;

    public float currentCooldown;

    public AudioClip[] attackClips;

    private void Start()
    {
        _relatedAnimator = GetComponent<Animator>();
    }

    public virtual void Update()
    {
        if (currentCooldown > 0)
            currentCooldown -= Time.deltaTime;
        else
            currentCooldown = 0f;
    }


    public virtual void Attack()
    {
        if (currentCooldown > 0f) return;

        SoundManager.PlaySound(attackClips[Random.Range(0, attackClips.Length)], 1f, false, 0f);

        _relatedAnimator.SetTrigger("Attack");
        _relatedAnimator.ResetTrigger("Move");
        currentCooldown = cooldown;
    }
}
