using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileKnife : MonoBehaviour
{
    public float speed;

    private void Start()
    {
        Destroy(gameObject, Random.Range(3f, 3f));
    }

    private void Update()
    {
        transform.Translate(Vector3.right/20f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Stats targetStats = collision.GetComponent<Stats>();

        if (!targetStats) return;

        targetStats.ModifyHealth(-1);

        Destroy(gameObject);
    }
}
