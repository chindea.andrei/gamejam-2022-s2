using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourAttackRanged : BehaviourAttackBase
{
    public GameObject projectile;
    public Transform shootPosition;
    public PlayerController _playerController;

    private Vector2 positionOnScreen;
    private Vector2 mouseOnScreen;

    public override void Update()
    {
        base.Update();
        Vector2 positionOnScreen = transform.position;
        Vector2 mouseOnScreen = _playerController._cam.ScreenToWorldPoint(Input.mousePosition);
        float angle = AngleBetweenTwoPoints(mouseOnScreen,positionOnScreen);
        shootPosition.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    public override void Attack()
    {
        if (currentCooldown > 0f) return;

        base.Attack();
        GameObject thisProjectile = Instantiate(projectile, shootPosition.position, shootPosition.rotation);

        Rigidbody2D rbKnife = thisProjectile.GetComponent<Rigidbody2D>();
        ProjectileKnife knife = thisProjectile.GetComponent<ProjectileKnife>();

        Vector3 dir = (positionOnScreen - mouseOnScreen).normalized;
    }
}
