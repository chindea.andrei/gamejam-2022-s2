using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    public static GameObject soundObject;

    private void Start()
    {
        //GetComponent<AudioSource>().Play();

        if (instance == null)
            instance = this;
        else
            DestroyImmediate(gameObject);
    }

    public static void PlaySound(AudioClip clip, float volume = 1f, bool loop = true, float pitch = 0f)
    {
        soundObject = new GameObject();
        soundObject.AddComponent<AudioSource>();
        soundObject.AddComponent<ItemSound>();
        AudioSource sound = soundObject.GetComponent<AudioSource>();
        sound.playOnAwake = false;
        sound.clip = clip;
        sound.volume = volume;
        sound.loop = loop;
        sound.pitch += Random.Range(-pitch, pitch);
        sound.gameObject.name = sound.clip.name;
        sound.Play();
    }

}
