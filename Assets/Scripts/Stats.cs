using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public float _speed;
    public int _health;
    public GameObject bloodParticle;

    public AudioClip[] damagedClips;
    public AudioClip[] deathClips;

    public void ModifyHealth(int damage)
    {
        _health += damage;
        SoundManager.PlaySound(damagedClips[Random.Range(0, damagedClips.Length)], 1f, false, 0f);

        if (_health <= 0) Death();
    }

    private void Death()
    {
        Debug.Log("Entity Died " + gameObject.name);
        Instantiate(bloodParticle, transform.position, Quaternion.identity);

        DropSystem drop = GetComponent<DropSystem>();
        if (drop) drop.DropItem();

        Destroy(gameObject);
    }

}
