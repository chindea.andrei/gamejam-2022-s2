using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Animator _playerAnimator;
    [SerializeField] private BehaviourAttackBase _attack;

    private SpriteRenderer _renderer;
    public Vector2 _moveTarget;

    [HideInInspector]
    public Camera _cam;

    private void Start()
    {
        _cam = Camera.main;
        _renderer = GetComponent<SpriteRenderer>();
    }
    private void Update()
    {

        if (Input.GetMouseButtonDown(1))
        {
            if (_attack.currentCooldown > 0f) return;

            Vector2 mousePosition = Input.mousePosition;
            _moveTarget = _cam.ScreenToWorldPoint(mousePosition);

            RaycastHit2D rayHit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(mousePosition));


            if (rayHit.collider != null)
            {
                ChangeScale(_moveTarget);
                ChangeState("Move");
            }
            else
                ChangeState("Idle");

        }

        if (Input.GetMouseButtonDown(0))
        {
            ChangeScale(_cam.ScreenToWorldPoint(Input.mousePosition));
            _attack.Attack();
        }
    }

    public void ChangeState(string stateName)
    {
        _playerAnimator.SetTrigger(stateName);
    }

    public void ResetState(string stateName)
    {
        _playerAnimator.ResetTrigger(stateName);
    }
    public void ChangeScale(Vector2 moveTarget)
    {
        _renderer.flipX = (transform.position.x < moveTarget.x) ? false : true;
    }
}
