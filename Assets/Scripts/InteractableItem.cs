using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableItem : InteractableBase
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Interact();
    }

    public override void Interact()
    {
        base.Interact();
        Destroy(gameObject);
    }
}
