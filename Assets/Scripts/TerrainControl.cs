using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainControl : MonoBehaviour
{
    public PlayerController player;
    public Camera cam;

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Vector2 mousePosition = Input.mousePosition;
            player._moveTarget = cam.ScreenToWorldPoint(mousePosition);
            player.ChangeState("Move");
        }
    }
}
