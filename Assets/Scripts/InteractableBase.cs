using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBase : MonoBehaviour
{
    //public PlayerController _playerController;

    public virtual void Interact()
    {
        Debug.Log("Interacted with " + gameObject.name);
    }
}
