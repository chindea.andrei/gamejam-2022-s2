using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropSystem : MonoBehaviour
{
    [System.Serializable]
    public class Drop
    {
        public GameObject item;
        public float dropChance;
    }

    public Drop[] drops;

    public void DropItem()
    {
        int randomItem = Random.Range(0, drops.Length);

        float chance = Random.Range(0f, 1f);
        if (chance < drops[randomItem].dropChance)
        {
            Instantiate(drops[randomItem].item, transform.position, Quaternion.identity);
        }
    }
    
}
