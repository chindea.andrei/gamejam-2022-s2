using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PDMove : StateMachineBehaviour
{
    public PlayerController _playerController;
    public Stats _playerStats;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _playerController = animator.GetComponent<PlayerController>();
        _playerStats = animator.GetComponent<Stats>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Vector2.Distance(_playerController.transform.position, _playerController._moveTarget) <= 0.01f)
        {
            _playerController.ChangeState("Idle");
            return;
        }

        _playerController.transform.position = 
            Vector2.MoveTowards(_playerController.transform.position, _playerController._moveTarget, 
            Time.deltaTime * _playerStats._speed);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _playerController.ResetState("Idle");
    }
}
